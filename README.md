# JavaConnexion

La connexion fonctionne, pour se connecter mettre son prenom en pseudo et son nom en mdp. Nos 3 nom/prenom sont dans le json de l'appli.
/!\ ne pas mettre de majuscule

* admin blandine danteuille
* user maneth seng
* user elias jalal

Actuellement la classe personne et le json ne contiennent que 3 attributs (roel,nom,prenom) il faut rajouter tt les autres attributs manquants.
Penser a ajouter les attributs dans la classe Personne, le Json et dans tt les controller qui en découle lors de la lecture, de l'écriture, de la création de la liste et de l'affichage des données dans la page user.

Il faut : 
* afficher la liste des personnes dans la tableView de la page d'admin
* coder l'inscription (ajouter un objet personne à la liste personne)
* coder l'oubli de mot de passe (generation d'un mot de passe aléatoire + update mdp dans la liste personne)
* coder toutes les vérifications des champs pour la connexion et l'inscription
